# Hacker News Dark

Dark mode for Hacker News.

![Screenshot previewing dark mode in Hacker News](hn-dark-screenshot.png)

# Questions you may have

## How do I use this?

Copy the contents of `hn.style.less` into your compatible styling addon. I'm using xStyle, but any of the others should work. Save it, and it should be dark.

## Why is the extension .less?

Because that's the format xStyle exported these modifications. It doesn't use any LESS functions/extensions, but it could possibly in the future.
